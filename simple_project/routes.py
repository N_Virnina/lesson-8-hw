from flask import render_template, request, url_for, session
from werkzeug.utils import redirect
import os
import hashlib

from simple_project import app
from db import get_connection


@app.route('/')
def root():
    user = session.get('user')
    return render_template('root.html', user=user)


@app.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        connection = get_connection(app.config['DB_NAME'])
        user_login = request.form['login']
        user_password = request.form['password']
        data = connection.execute(
            f"SELECT username, name, password, salt from users where username = ?", (user_login,))
        dt = list(data)
        if len(dt) == 0:
            return render_template('login.html', message='No such user/password in database')
        if len(dt) > 1:
            return render_template('login.html', message='There more than one user with this username')
        username, name, password, salt = dt[0]
        user_hex = hashlib.pbkdf2_hmac('sha512', bytes(user_password, 'utf-8'), salt, 120000)
        if user_hex == password:
            session.clear()
            session['user'] = {'login': username, 'name': name}
            connection.close()
            return redirect(url_for('root'))
        else:
            return render_template('login.html', message='No such user/password in database')
    user = session.get('user')
    if user is not None:
        return redirect(url_for('root'))
    return render_template('login.html')


@app.route('/signup', methods=('GET', 'POST'))  # registration
def register():
    if request.method == 'POST':
        user_login = request.form['login']
        user_name = request.form['name']
        user_password = request.form['password']
        user_salt = os.urandom(32)
        user_hex = hashlib.pbkdf2_hmac('sha512', bytes(user_password, 'utf-8'), user_salt, 120000)
        connection = get_connection(app.config['DB_NAME'])
        cursor = connection.cursor()
        data = connection.execute(f"SELECT username FROM users where username = '{user_login}' ")
        dt = list(data)
        if len(dt) >= 1:
            return render_template('register.html', message='This login is already used by another user')
        insertion = f'''
                INSERT INTO users (username, name, password, salt) values 
                (?, ?, ?, ?);
            '''
        cursor.execute(insertion, (user_login, user_name, user_hex, user_salt))
        connection.commit()
        connection.close()
        return redirect(url_for('root'))
    return render_template('register.html')


@app.route('/logout', methods=('GET', 'POST'))
def logout():
    if 'user' not in session:
        return redirect(url_for('login'))
    session.clear()
    return redirect(url_for('root'))


@app.errorhandler(500)
def some_error(e):
    return render_template('errors/500.html'), 500


@app.route('/error')
def error():
    raise ValueError()
