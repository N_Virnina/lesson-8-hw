from sqlite3 import OperationalError

from simple_project import app
import routes

if __name__ == '__main__':
    app.run()
