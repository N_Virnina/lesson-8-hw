from flask import Flask

from config import MyFlaskConfig as mfc

app = Flask(__name__)
app.config.from_object(mfc)
