import sqlite3 as sql


def get_connection(db_name):
    conn = sql.connect(db_name)
    return conn

