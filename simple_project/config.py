class MyFlaskConfig:
    DB_NAME = 'people.db'
    DEBUG = False
    SECRET_KEY = 'some_unknown_key_for_session'
