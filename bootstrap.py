import sqlite3 as sql


connection = sql.connect('simple_project/people.db')
cursor = connection.cursor()
cursor.execute('''
    CREATE TABLE users(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT NOT NULL,
        name TEXT NOT NULL,
        password TEXT NOT NULL,
        salt BLOB);
    ''')
connection.commit()
connection.close()
